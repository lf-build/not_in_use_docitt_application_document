﻿using Moq;
using Microsoft.AspNet.Http;
using System.IO;
using Xunit;
using Docitt.Application.Document.Api.Controllers;
using LendFoundry.DocumentManager;
using Docitt.Application.Document.Api.ActionResults;
using System;

namespace Docitt.Application.Document.Api.Tests
{
    public class ApplicationDocumentControllerTest
    {
        Mock<IApplicationDocumentService> applicationdocumentService;
   

        ApplicationDocumentController Api;
        public ApplicationDocumentControllerTest()
        {
            applicationdocumentService = new Mock<IApplicationDocumentService>();

            Api = new ApplicationDocumentController(applicationdocumentService.Object);
        }


        private ApplicationDocumentController GetApplicationDocumentController
        {
            get
            {
                var documentManagerService = new Mock<IDocumentManagerService>();

                IApplicationDocumentService applicationdocumentService = new ApplicationDocumentService(
                    documentManagerService.Object, Mock.Of<Configuration>());

                return new ApplicationDocumentController(applicationdocumentService);
            }
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ApplicationDocumentController(null));
        }

        [Fact]
        public void DownloadDocument_ValidApplicationNumber_ReturnsResult()
        {
            //var controller = GetApplicationDocumentController;
            //var result = controller.DownloadDocumentsByApplicationNumber("0001").Result as FileActionResult;
            var result = Api.DownloadDocumentsByApplicationNumber("0001").Result as FileActionResult;
            Assert.NotNull(result);
        }
    }
}
