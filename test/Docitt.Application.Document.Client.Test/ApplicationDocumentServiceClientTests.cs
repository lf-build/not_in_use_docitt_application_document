﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Docitt.Application.Document.Client.Test
{
    public class ApplicationDocumentServiceClientTests
    {
        private IApplicationDocumentService ApplicationDocumentServiceClient { get; }
        private IRestRequest request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }
        
     
        public ApplicationDocumentServiceClientTests()
        {
            //Request = new RestRequest();
            MockServiceClient = new Mock<IServiceClient>();
            ApplicationDocumentServiceClient = new ApplicantionDocumentServiceClient(MockServiceClient.Object);
        }

    

        [Fact]
        public void Client_DownloadDocumentsByApplicationNumber()
        {
            IRestRequest request = null;

        

            MockServiceClient.Setup(s => s.ExecuteAsync<DownloadZipResponse>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r)
                .ReturnsAsync(new DownloadZipResponse());

            var result = ApplicationDocumentServiceClient.DownloadDocumentsByApplicationNumber("000001").Result;

            //Assert.Equal(expected, result);
            Assert.Equal("{applicationNumber}/download", request.Resource);
            Assert.Equal(Method.GET, request.Method);
        }

    }
}
