FROM registry.lendfoundry.com/base:beta8

LABEL RevisionNumber "<<RevisionNumber>>"

ADD ./src/Docitt.Application.Document.Abstractions /app/Docitt.Application.Document.Abstractions
WORKDIR /app/Docitt.Application.Document.Abstractions
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/Docitt.Application.Document /app/Docitt.Application.Document
WORKDIR /app/Docitt.Application.Document
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

ADD ./src/Docitt.Application.Document.Api /app/Docitt.Application.Document.Api
WORKDIR /app/Docitt.Application.Document.Api
RUN dnu restore --ignore-failed-sources --no-cache -s http://build.lendfoundry.co/guestAuth/app/nuget/v1/FeedService.svc/  -s https://www.nuget.org/api/v2/ -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel