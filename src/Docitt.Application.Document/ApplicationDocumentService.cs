﻿using LendFoundry.DocumentManager;
using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Docitt.Application.Document
{
    public class ApplicationDocumentService : IApplicationDocumentService
    {
        #region Constructor

        public ApplicationDocumentService(IDocumentManagerService documentManagerService,Configuration config)
        {
            if (documentManagerService == null) throw new ArgumentException($"{nameof(documentManagerService)} is mandatory");
            if (config == null) throw new ArgumentException($"{nameof(config)} is mandatory");
            DocumentManager = documentManagerService;
            Configuration = config;
        }

        #endregion

        #region Private Variables

        private Configuration Configuration { get; }
        private IDocumentManagerService DocumentManager { get; }
        #endregion
        
        public async Task<DownloadZipResponse> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new ArgumentException("Argument is null or whitespace", nameof(applicationNumber));

            //get all the documents to be downloaded
            var documents = await DocumentManager.GetAll("application", applicationNumber);
            
            using (var outStream = new MemoryStream())
            {
                using (var archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    //for (int fileCount = 0; fileCount < documents.Count(); fileCount++)
                    foreach(var document in documents)
                    {
                        //download the document from document manager
                        var documentContent = await DocumentManager.Download("application", applicationNumber, document.Id);
                        var fileInArchive = archive.CreateEntry(document.Id +"_" + document.FileName);
                        using (var entryStream = fileInArchive.Open())
                        using (var fileToCompressStream = new MemoryStream(ConvertStreamToBytes(documentContent)))
                        {
                            fileToCompressStream.CopyTo(entryStream);
                        }
                    }
                }
                var downloadZipResponse = new DownloadZipResponse();
                downloadZipResponse.ZipFileName = Configuration.DownloadZipFileName;
                downloadZipResponse.ZipFileData = outStream.ToArray();
                return downloadZipResponse;
            }
        }

       
        private static byte[] ConvertStreamToBytes(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        
    }
}
