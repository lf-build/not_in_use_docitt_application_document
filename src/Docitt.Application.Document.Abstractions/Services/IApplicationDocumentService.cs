﻿using System.Threading.Tasks;

namespace Docitt.Application.Document
{
    public interface IApplicationDocumentService
    {
        #region Applicant- Application Documents

        

        Task<DownloadZipResponse> DownloadDocumentsByApplicationNumber(string applicationNumber);


     
        #endregion
    }
}
