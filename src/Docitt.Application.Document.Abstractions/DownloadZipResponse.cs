﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Application.Document
{
    public class DownloadZipResponse
    {
       public string ZipFileName { get; set; }
       public byte[] ZipFileData { get; set; }
    }
}
