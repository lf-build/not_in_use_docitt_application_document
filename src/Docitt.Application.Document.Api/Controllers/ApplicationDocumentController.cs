﻿
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Logging;

namespace Docitt.Application.Document.Api
{
    [Route("/")]
    public class ApplicationDocumentController : ExtendedController
    {
        public ApplicationDocumentController(IApplicationDocumentService applicationDocument, ILogger logger) : base(logger)
        {
            ApplicationDocumentService = applicationDocument;
            if (ApplicationDocumentService == null)
                throw new ArgumentException($"{nameof(ApplicationDocumentService)} is mandatory");
        }

        private IApplicationDocumentService ApplicationDocumentService { get; }

        /// <summary>
        /// DownloadDocumentsByApplicationNumber
        /// </summary>
        /// <param name="applicationNumber">applicationNumber</param>
        /// <returns>FileActionResult</returns>
        [HttpGet("{applicationNumber}/download")]        
        [ProducesResponseType(typeof(FileActionResult),200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            try
            {
                var zipFileResponse = await ApplicationDocumentService.DownloadDocumentsByApplicationNumber(applicationNumber);
                return new FileActionResult(zipFileResponse.ZipFileName, zipFileResponse.ZipFileData);
            }
            catch (ArgumentException ex)
            {
                return ErrorResult.BadRequest(ex.Message);
            }            
            catch (InvalidOperationException)
            {
                return ErrorResult.BadRequest("Cannot generate zip file for the documents");
            }
        }
    }
}
