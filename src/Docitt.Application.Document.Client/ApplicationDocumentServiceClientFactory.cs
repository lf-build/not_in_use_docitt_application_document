﻿
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;

namespace Docitt.Application.Document.Client
{
    public class ApplicationDocumentServiceClientFactory : IApplicationDocumentServiceClientFactory
    {
        public ApplicationDocumentServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IApplicationDocumentService Create(ITokenReader reader)
        {          
            var client = Provider.GetServiceClient(reader, Endpoint, Port);
            return new ApplicantionDocumentServiceClient(client);           
        }
    }
}
