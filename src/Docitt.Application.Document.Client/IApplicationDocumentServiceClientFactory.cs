﻿
using LendFoundry.Security.Tokens;

namespace Docitt.Application.Document.Client
{
    public interface IApplicationDocumentServiceClientFactory
    {
        IApplicationDocumentService Create(ITokenReader reader);
    }
}
