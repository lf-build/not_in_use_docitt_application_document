﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace Docitt.Application.Document.Client
{
    public static class ApplicationDocumentServiceClientExtensions
    {
        public static IServiceCollection AddApplicantDocumentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationDocumentServiceClientFactory>(p => new ApplicationDocumentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationDocumentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
