﻿using LendFoundry.Foundation.Services;
using RestSharp;
using System.Threading.Tasks;

namespace Docitt.Application.Document.Client
{
    public class ApplicantionDocumentServiceClient : IApplicationDocumentService
    {
        public ApplicantionDocumentServiceClient(IServiceClient client)
        {
            Client = client;
        }
        private IServiceClient Client { get; }
        public async Task<DownloadZipResponse> DownloadDocumentsByApplicationNumber(string applicationNumber)
        {
            var objRequest = new RestRequest("{applicationNumber}/download", Method.GET);
            objRequest.AddUrlSegment("applicationNumber", applicationNumber);
            return (await Client.ExecuteAsync<DownloadZipResponse>(objRequest));
        }
    }
}
