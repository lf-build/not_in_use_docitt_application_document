# Set-ExecutionPolicy unrestricted

Add-Type -AssemblyName System.IO.Compression.FileSystem

function Unzip([string]$zipfile, [string]$outpath)
{
    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

function Download-And-Unzip-Dependencies([string]$Dependency, [string]$Url, [string]$OutPath, [string]$FinalPath)
{    
    echo ('Downloading ' + $Dependency)
    wget $Url -OutFile $OutPath | Out-Null
    echo 'OK'

    echo ('Unzip ' + $Dependency)
    Unzip -zipfile ((Resolve-Path .\).Path +  "\" + $OutPath) -outpath  ((Resolve-Path .\).Path +  "\" + $FinalPath)
    echo 'OK'
}

function Install-Coverage-Dependencies([string]$CoverageFolder)
{
    echo 'Instaling dependencies...'

    echo ('Creating folder ' + $CoverageFolder)
    New-Item -ItemType Directory -Force -Path ($CoverageFolder + '\downloads') | Out-Null
    New-Item -ItemType Directory -Force -Path ($CoverageFolder  + '\open_cover') | Out-Null
    New-Item -ItemType Directory -Force -Path ($CoverageFolder  + '\report_generator') | Out-Null
    echo 'OK'
    
    Download-And-Unzip-Dependencies -Dependency 'Opencover' -Url "https://github.com/OpenCover/opencover/releases/download/4.6.519/opencover.4.6.519.zip" -OutPath  '.coverage\downloads\opencover.zip' -FinalPath '.coverage\open_cover'
    Download-And-Unzip-Dependencies -Dependency 'ReportGenerator' -Url 'https://github.com/danielpalme/ReportGenerator/releases/download/v2.4.5.0/ReportGenerator_2.4.5.0.zip' -OutPath '.coverage\downloads\reportgenerator.zip' -FinalPath '.coverage\report_generator'

    echo 'Installation finished!'
}

function Check-Coverage-Instalation
{
    return [System.IO.File]::Exists(((Resolve-Path .\).Path +  "\" + $OPEN_CONSOLE)) -and [System.IO.File]::Exists(((Resolve-Path .\).Path +  "\" + $REPORT_GENERATOR))
}

function Add-To-Gitignore
{
    $gitignore = '.gitignore'
    $file = Get-Content $gitignore
    $containsWord = $file | %{ $_ -match '.coverage/' }
    If(!($containsWord -contains $true))
    {
        echo 'Adding dependency folder to .gitignore'
        Add-Content $gitignore ''
        Add-Content $gitignore '# Folder of coverage report dependencies'
        Add-Content $gitignore  '.coverage/'
        Add-Content $gitignore ''
    }
}


function Build-Output-Node($Xml)
{
     $Child = $Xml.CreateElement("ProduceOutputsOnBuild", $Xml.DocumentElement.NamespaceURI)
     $Child.InnerText = 'True' 
     return $Child;
}

function Produces-Output-On-Build([array]$SrcProjects)
{

    Foreach ($Project in $SrcProjects) 
    {        
        $Path = ((Resolve-Path .\).Path + "/src/" + $Project + "/" + $Project + ".xproj")
        $Xml = [xml](Get-Content $path)
       
        $checkForRegister = $Xml.Project.PropertyGroup |
            Foreach-Object { $_.ProduceOutputsOnBuild }
        
        if(!($checkForRegister -contains $true))
        {
            # This iteration is used when the developer marked and dismarked the options
            # produces the build output
            Foreach($PropertyGroup in $Xml.Project.PropertyGroup)
            {
                if($PropertyGroup.Condition)
                {
                    $PropertyGroup.AppendChild((Build-Output-Node($Xml)))
                    $Xml.Save($path)
                    return
                }
            }

            # if developer never marked 'Produces output build'
            $PropertyGroup = $Xml.CreateElement("PropertyGroup", $Xml.DocumentElement.NamespaceURI)
            $PropertyGroup.SetAttribute("Condition","'`$(Configuration)|`$(Platform)'=='Debug|AnyCPU'")
            $PropertyGroup.AppendChild((Build-Output-Node($Xml)))
            $Xml.Project.AppendChild($PropertyGroup)
            $Xml.Save($path)
        }
    }
}



## start ## 

# setup dependencies settings

$COVERAGE_FOLDER = '.coverage'
$OPEN_CONSOLE = ($COVERAGE_FOLDER + '\open_cover\OpenCover.Console.exe')
$REPORT_GENERATOR = ($COVERAGE_FOLDER + '\report_generator\bin\ReportGenerator.exe')
$COVERAGE_OUTPUT= '.\artifacts\coverage\coverage.xml'
$DNX_PATH = Get-command dnx | Select-Object -ExpandProperty Definition
$XUNIT_FILTER= """+[*]*-[xunit*]*"""

# setup test settings

$TEST_ROOT = 'test'
$TEST_PROJECTS = Get-ChildItem $TEST_ROOT | 
       Where-Object {$_.PSIsContainer} | 
       Foreach-Object {  $TEST_ROOT + '\' + $_.Name }

# setup src settings 

$SRC_ROOT = 'src'
$SRC_PROJECTS = Get-ChildItem $SRC_ROOT | 
       Where-Object {$_.PSIsContainer} | 
       Foreach-Object {  $_.Name }

Produces-Output-On-Build -SrcProjects $SRC_PROJECTS # set src output on artifacts on build by VS2015

# set dll folders

$BIN_ROOT = 'artifacts\bin'

$BIN_POSTFIX = '\Debug\dnx451' # if your runtime is not dnx451, change here

$BIN_DIRS = Get-ChildItem $BIN_ROOT | 
       Where-Object {$_.PSIsContainer} | 
       Foreach-Object { $BIN_ROOT + '\' + $_.Name + $BIN_POSTFIX } 
$BIN_DIRS_STR = $BIN_DIRS | 
       & { $ofs=';'; "$input" }


# check if coverage dependencies ar installed

If(!(Check-Coverage-Instalation))
{
    dnvm use 1.0.0-beta8
    # if setup is true, skip the report generations
    Install-Coverage-Dependencies -CoverageFolder $COVERAGE_FOLDER # install dependencies
    Add-To-Gitignore # add dependencies folders to gitignore
    echo 'Please re-build your solution on VS2015!' # after setup setup is completed, the solution requires a rebuild
    exit;
}

New-Item -ItemType Directory -Force -Path "artifacts\coverage" | Out-Null

# for each test project run the opencover and merge it at output file (xml)
ForEach ($Project in $TEST_PROJECTS) 
{
    $target = " -target:" + $DNX_PATH
    $targetargs = ' "-targetargs: -p ' + $Project + ' test"'
    $searchdirs = " -searchdirs:""" +  $BIN_DIRS_STR  + """"
    $register = " -register:user"
    $output = " -output:" + $COVERAGE_OUTPUT
    $filter= " -filter:" + $XUNIT_FILTER
    $mergeoutput = " -mergeoutput"

    Invoke-Expression ($OPEN_CONSOLE + $target + $targetargs + $searchdirs + $register + $output + $filter + $mergeoutput)
    echo ($OPEN_CONSOLE + $target + $targetargs + $searchdirs + $register + $output + $filter + $mergeoutput)
}

# settings for coverage report

$reports = " -reports:""" + $COVERAGE_OUTPUT + """"
$targetdir = " -targetdir:""artifacts\coverage\report"""

New-Item -ItemType Directory -Force -Path "artifacts\coverage\report" | Out-Null

Invoke-Expression($REPORT_GENERATOR + $reports + $targetdir) # build report based on generated xml at artifact/report

# if you want to see the report 
$option = Read-Host 'Do you want to see the report? (y/n)?'

If($option -eq "Y" -or $option -eq "y")
{
    start "artifacts\coverage\report\index.htm"
} 

## end ##